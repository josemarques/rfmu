#!/usr/bin/env python

#TSFI -> Touchstone file importer

import sys
sys.path.append('..')

import math
import cmath

from SUBM.CSV import CSV
from SUBM.MATH.PY import LNAL

from PY import IMNT

class TOUCHST:
	#Touchstone data object
	def __init__(self, filename, nports=2, nic=3, token="\t", headsize=2):
		self.CSVF=CSV.CSV(filename,token,headsize)

		self.NPORTS=nports #Number of ports
		self.FRQM=1e6#Frequency multiplier
		self.Z0=50+0j #Characteristic impedance
		self.DCOD="MA"#Data codification: "MA" module argument, "RI" real imaginary.

		self.FRQ=[] #Frequency data avaliable
		self.S=[] #S parameter matrix

		self.Z=[] #Z parameter matrix

		self.NIC=nic #Network impedance configuration
		self.IMP=[] #Impedance vector

	def TOKID(self,lasthead):#Token identifier. Search for the CSV used token
		if (lasthead.find("\t") != -1):#Tab found
			#print("TAB TOKEN")
			return "\t"
		elif (lasthead.find(";") != -1):#Semicolon found
			return ";"
		elif (lasthead.find(" ") != -1):#Space found
			#print("SPACE TOKEN")
			return " "
		else:
			raise NameError('No token found on touchstone file')

	def HEDPR(self):#Toutchstone file header processor
		STRHEAD=self.CSVF.STRDATA.splitlines()
		self.CSVF.HEAD=[]
		SETTINGS=[]
		HEADSIZE=0
		for i in range(len(STRHEAD)):
			if (STRHEAD[i][0]=="!"):#Head line
				HEADSIZE=HEADSIZE+1
				self.CSVF.HEAD.append(STRHEAD[i].split())
			elif (STRHEAD[i][0]=="#"):#Setings head line
				HEADSIZE=HEADSIZE+1
				SETTINGS=STRHEAD[i].split()
				#print(SETTINGS)

			else:#If dont found comments there is no more head data
				break
		#print("TOCK SEARCH LINE")
		#print(STRHEAD[HEADSIZE])
		self.CSVF.TOKEN=self.TOKID(STRHEAD[HEADSIZE])
		#print("self.CSVF.TOKEN")
		#print(self.CSVF.TOKEN)

		#Use head size data to decode CSV
		self.CSVF.HEADSIZE=HEADSIZE
		self.CSVF.DECO()
		#print("self.CSVF.DATA")
		#print(self.CSVF.DATA)
		#Use head size data to decode CSV

		if(len(SETTINGS)>0):#If there are settings
			if(SETTINGS[1]=="Hz"):
				self.FRQM=1
			elif(SETTINGS[1]=="MHz"):
				self.FRQM=1e6
			elif(SETTINGS[1]=="GHz"):
				self.FRQM=1e9

	def MTXC (self):#Matrix calculations
		#Clean matrices and vectors
		self.FRQ=[]
		self.S=[]
		self.Z=[]
		self.IMP=[]
		#Clean matrices and vectors
		#Data processor
		for i in range(len(self.CSVF.DATA)):#Data point iterator
			self.FRQ.append(self.CSVF.DATA[i][0]*self.FRQM)#First value frequency
			#S matrix extractor
			tSV=[]
			for ii in range(1,len(self.CSVF.DATA[i]),2):#S matrix complex value constructor
				Siir=self.CSVF.DATA[i][ii]*math.cos(self.CSVF.DATA[i][ii+1]*math.pi/180)
				Siii=self.CSVF.DATA[i][ii]*math.sin(self.CSVF.DATA[i][ii+1]*math.pi/180)
				tSV.append(complex(Siir,Siii))
			tSM=[]
			DIM=int(len(tSV)**0.5)
			for ii in range(DIM):#S matrix assembler
				ttSM=[]#COLUMNS
				for iii in range(DIM):#S matrix assembler
					ttSM.append(tSV[iii+ii*DIM])
				tSM.append(ttSM)
			self.S.append(LNAL.TM(tSM))
			#S matrix extractor
			#Z matrix calculation
			self.Z.append(IMNT.SMZM(self.S[len(self.S)-1]))
			print()
			#Z matrix calculation
			#IMP calculation
			if(self.NIC==3):
				self.IMP.append(IMNT.ZMZ12(self.Z[len(self.Z)-1]))

