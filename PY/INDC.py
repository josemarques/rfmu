# -*- coding: utf-8 -*-
#!/usr/bin/env python

#INDC.py -> Inductor lib
import sys
sys.path.append('..')
from SUBM.MATH.PY import LNAL
from SUBM.CSV import CSV
pi=3.14159265359
eps=1E-3

class EXIND:#Experimental measured inductor
	Z0r=50#Parte real de la impedancia característica
	Z0i=0#Parte imaginaria de la impedancia cvaracterística

	mL=1e-9#Multiplicador de la aiutoinductancia "nH"
	mC=1e-12#Multiplicador capacitancia "pF"

	def __init__(filename): #Need to initialize self variables in __init__?
		self.CSVO=CSV()

		self.F=[]#Frequency for wich impedance data is avaliable
		self.Z=[]#Impedance values. Complex numbers
		self.eZ=[]#Standart deviation of impedance values if avaliable

		self.SRF=[]#Array with diferent SRF
		self.eSRF=[]#Standar devition of SRF if avaliable

		self.L=[]#Frequency dependent inductance
		self.eL=[]#Standard deviation of frequency dependent autoinductance if avaliable
		self.C=[]#Frequency dependnt capacitance
		self.eC=[]#Standard deviation of frequency dependent capacitance if avaliable

	def STATS(self):
		MSGREPR="#############################################"+"\n"

		MSGREPR=MSGREPR+"SRF: "+str(self.SRF)+"\n"

		"""
		MSGREPR=MSGREPR+"F\tR\tRds\tX\tXds"+"\n"
		for i in range(0, len(self.F)):
			MSGREPR=MSGREPR+str(self.F[i])+"\t"
			MSGREPR=MSGREPR+str(self.zR[i])+"\t"
			MSGREPR=MSGREPR+str(self.eZR[i])+"\t"
			MSGREPR=MSGREPR+str(self.zX[i])+"\t"
			MSGREPR=MSGREPR+str(self.eZX[i])+"\t"

			MSGREPR=MSGREPR+"\n"
		#"""
		MSGREPR=MSGREPR+"#############################################"+"\n"

		return MSGREPR

	def __repr__(self):
		return self.STATS()

	def __str__(self):
		return self.STATS()


	def CSRF(self):#Compute SRF
		self.SRF=MMRF.SRCH_SRF(self.F,self.zX)
		print("SRF0"+str(self.SRF))


		#print([list(range(0,len(self.SRF))),self.SRF])
		#print(LNAL.TM([range(0,len(self.SRF)),self.SRF]))
		#self.SRF=SIGPR.FQCM([list(range(0,len(self.SRF))),self.SRF],100*1e6)[1]#Remove high frequency SRF. Possible in noisy data. Only expect 1 SRF on 100 MHZ spans

		print("SRF1"+str(self.SRF))

		ezxa=MMRF.sLIST(self.zX,self.ezX)
		#print("self.zX::"+str(self.zX))
		#print("ezxa::"+str(ezxa))

		esrfa=MMRF.SRCH_SRF(self.F,ezxa)
		print(esrfa)
		#esrfa=SIGPR.FQCM([list(range(0,len(esrfa))),esrfa],100*1e6)[1]#Remove high frequency SRF. Possible in noisy data. Only expect 1 SRF on 100 MHZ spans

		print(esrfa)

		print("self.SRF::"+str(self.SRF))
		print("esrfa::"+str(esrfa))

		#TODO Solve this next work around. Assure the assertion the proper way

		while (len(esrfa)!=len(self.SRF)):
			if(len(esrfa)>len(self.SRF)):
				del esrfa[-1]
			elif(len(esrfa)<len(self.SRF)):
				del self.SRF[-1]

		assert len(esrfa)==len(self.SRF), "Number of SRF points is not the same taking standart deviation in consideration"

		self.eSRF=MMRF.rLIST(self.SRF,esrfa)
		#print("self.eSRF_0:"+str(self.eSRF))
		self.eSRF=MMRF.aLIST(self.eSRF)
		#print("self.eSRF_1::"+str(self.eSRF))

	def CL(self):#Compute inductance
		self.L=[]
		for i in range(0,len(self.F)):
			self.L.append(self.zX[i]/(2*pi*self.F[i]))

	def CC(self):#Compute capacitance
		self.C=[]
		for i in range(0,len(self.F)):
			self.C.append(-self.zX[i]/(2*pi*self.F[i]))

	def LFCL(self):#Low frequency compute inductance
		self.CSRF()
		FFRQ=self.SRF[0]/2
		LIMITA=MMRF.FND_V_L(self.F,FFRQ)
		self.LFL=0
		for i in range(0,int(LIMITA[0])):
			self.LFL=self.LFL+self.L[i]

		self.LFL=self.LFL/LIMITA[0]
		#self.LFL=self.LFL*self.mL

	def LFCC(self):#Low frecuency capacitance compute
		self.LFCL()
		self.LFC=(1/((self.LFL)*(self.SRF[0]*2*pi)**2))
		#self.LFC=self.LFC*self.mC

	def LFCR(self):#Low frequency compute resistance
		self.LFR=self.zR[0]


class PISM:#Planar inductor simplified model
	Z0r=50#Parte real de la impedancia característica
	Z0i=0#Parte imaginaria de la impedancia cvaracterística

	mL=1e-9#Multiplicador de la aiutoinductancia "nH"
	mC=1e-12#Multiplicador capacitancia "pF"

	def __init__(self, f, z, ez=[], nbp="TA1", src="NANOVNA", nme="10ug"):
		self.nBP=nbp#Designación del modelo de bobina plana
		self.sRC=src#Designación de la fuente de los datos
		self.nME=nme#Designación de material en zona sensible

		self.F=list(f)#Frequency for wich impedance data is avaliable
		self.Z=list(z)#Mean values of the impedance
		self.eZ=list(ez)#Standart deviatio the impedance

		self.SRF=[]#Array with SRF points
		self.eSRF=[]#Standar devition of SRF points if avaliable

		self.L=[]#Inductive behaviour
		self.C=[]#Capacitive behaviour

		self.LFL=0#Low frequency autoinductance
		self.LFC=0#Low frequenci capacitance
		self.LFR=0#Low frequency resistance


		self.CSRF()
		self.CL()
		self.CC()

		#This broke for some reason
		#self.LFCL()
		#self.LFCC()
		#self.LFCR()

	def STATS(self):
		MSGREPR="#############################################"+"\n"
		MSGREPR=MSGREPR+"Modelo de bobina: "+str(self.nBP)+"\n"
		MSGREPR=MSGREPR+"Fuente de datos: "+str(self.sRC)+"\n"
		MSGREPR=MSGREPR+"Designación de muestra: "+str(self.nME)+"\n"

		MSGREPR=MSGREPR+"SRF: "+str(self.SRF)+"\n"

		"""
		MSGREPR=MSGREPR+"F\tR\tRds\tX\tXds"+"\n"
		for i in range(0, len(self.F)):https://gitlab.com/josemarques/math.git
			MSGREPR=MSGREPR+str(self.F[i])+"\t"
			MSGREPR=MSGREPR+str(self.zR[i])+"\t"
			MSGREPR=MSGREPR+str(self.eZR[i])+"\t"
			MSGREPR=MSGREPR+str(self.zX[i])+"\t"
			MSGREPR=MSGREPR+str(self.eZX[i])+"\t"

			MSGREPR=MSGREPR+"\n"
		#"""
		MSGREPR=MSGREPR+"#############################################"+"\n"

		return MSGREPR

	def __repr__(self):
		return self.STATS()

	def __str__(self):
		return self.STATS()


	def CSRF(self):#Compute SRF
		self.SRF=MMRF.SRCH_SRF(self.F,LNAL.Vimag(self.Z))

		#ezxa=MMRF.sLIST(self.zX,self.ezX)
		#print("self.zX::"+str(self.zX))
		#print("ezxa::"+str(ezxa))

		#esrfa=MMRF.SRCH_SRF(self.F,ezxa)

		#print("self.SRF::"+str(self.SRF))
		#print("esrfa::"+str(esrfa))

		#ssert len(esrfa)==len(self.SRF), "Number of SRF points is not the same taking standart deviation in consideration"

		#self.eSRF=MMRF.rLIST(self.SRF,esrfa)
		#print("self.eSRF_0:"+str(self.eSRF))
		#self.eSRF=MMRF.aLIST(self.eSRF)
		#print("self.eSRF_1::"+str(self.eSRF))

	def CL(self):#Compute inductance
		self.L=[]
		for i in range(0,len(self.F)):
			self.L.append(LNAL.Vimag(self.Z)[i]/(2*pi*self.F[i]))

	def CC(self):#Compute capacitance
		self.C=[]
		for i in range(0,len(self.F)):
			self.C.append(-LNAL.Vimag(self.Z)[i]/(2*pi*self.F[i]))

	def LFCL(self):#Low frequency compute inductance
		self.CSRF()
		#print(self.SRF)
		FFRQ=self.SRF[0]/2
		#print(FFRQ)
		LIMITA=MMRF.FND_V_L(self.F,FFRQ)
		#print(LIMITA)
		self.LFL=0
		for i in range(0,int(LIMITA[0])):
			self.LFL=self.LFL+self.L[i]

		self.LFL=self.LFL/LIMITA[0]
		#self.LFL=self.LFL*self.mL

	def LFCC(self):#Low frecuency capacitance compute
		self.LFCL()
		self.LFC=(1/((self.LFL)*(self.SRF[0]*2*pi)**2))
		#self.LFC=self.LFC*self.mC

	def LFCR(self):#Low frequency compute resistance
		self.LFR=LNAL.Vreal(self.Z)[0]


class MSBP:#Modelo simplificado de bobina plana
	Z0r=50#Parte real de la impedancia característica
	Z0i=0#Parte imaginaria de la impedancia cvaracterística

	mL=1e-9#Multiplicador de la aiutoinductancia "nH"
	mC=1e-12#Multiplicador capacitancia "pF"

	def __init__(self, nbp="TA1", nme="0_B", f=1E6, r=0.1, l=50, ur=1, ui=eps, c=1, er=1, ei=eps):
		self.nBP=nbp#Designación del modelo de bobina plana
		self.nME=nme#Designación de material en zona sensible

		self.F=f

		self.R=r

		self.L=l
		self.uR=ur
		self.uI=ui

		self.C=c
		self.eR=er
		self.eI=ei

		self.zR=0
		self.zX=0

		self.nR=1
		self.nI=1

	def STATS(self):
		MSGREPR="#############################################"+"\n"
		MSGREPR=MSGREPR+"Modelización de bobina plana. ::Cálculo::\n"
		MSGREPR=MSGREPR+"Modelo de bobina: "+str(self.nBP)+"\n"
		MSGREPR=MSGREPR+"Designación de muestra: "+str(self.nME)+"\n"

		MSGREPR=MSGREPR+"R: "+str(self.R)+"\n"

		MSGREPR=MSGREPR+"L: "+str(self.L)+"\n"
		MSGREPR=MSGREPR+"uR: "+str(self.uR)+"\n"
		MSGREPR=MSGREPR+"uI: "+str(self.uI)+"\n"

		MSGREPR=MSGREPR+"C: "+str(self.C)+"\n"
		MSGREPR=MSGREPR+"eR: "+str(self.eR)+"\n"
		MSGREPR=MSGREPR+"eI: "+str(self.eI)+"\n"

		self.N()

		MSGREPR=MSGREPR+"nR: "+str(self.nR)+"\n"
		MSGREPR=MSGREPR+"nI: "+str(self.nI)+"\n"

		"""
		if isinstance(self.F, list):#Si la respuesta se busca para un arrray de frecuencias
			MSGREPR=MSGREPR+"F\tR\tX"+"\n"
			for i in range(0, len(self.F)):
				MSGREPR=MSGREPR+str(self.F[i])+"\t"
				MSGREPR=MSGREPR+str(self.zR[i])+"\t"
				MSGREPR=MSGREPR+str(self.zX[i])+"\t"

				MSGREPR=MSGREPR+"\n"

		else:
			MSGREPR=MSGREPR+"F\tR\tX"+"\n"
			MSGREPR=MSGREPR+str(self.F)+"\t"
			MSGREPR=MSGREPR+str(self.zR)+"\t"
			MSGREPR=MSGREPR+str(self.zX)+"\t"

			MSGREPR=MSGREPR+"\n"

		#"""

		MSGREPR=MSGREPR+"#############################################"+"\n"

		return MSGREPR

	def __repr__(self):
	#def __str__(self):

		return self.STATS()

	def ZEQr(self,f=0, l=10, c=1, ZCR=0.001, ur=1, ui=0.001,  er=1, ei=0.001):
		L=l
		C=c

		return (((2*pi*L*f*ui)/(4*pi**2*L**2*f**2*ur**2+4*pi**2*L**2*f**2*ui**2)+2*pi*C*f*ei)/((2*pi*C*f*er-(2*pi*L*f*ur)/(4*pi**2*L**2*f**2*ur**2+4*pi**2*L**2*f**2*ui**2))**2+((2*pi*L*f*ui)/(4*pi**2*L**2*f**2*ur**2+4*pi**2*L**2*f**2*ui**2)+2*pi*C*f*ei)**2)+ZCR)

	def ZEQx(self,f=0, l=10, c=1, ZCR=0.001, ur=1, ui=0.001,  er=1, ei=0.001):
		L=l
		C=c

		return (-(2*pi*C*f*er-(2*pi*L*f*ur)/(4*pi**2*L**2*f**2*ur**2+4*pi**2*L**2*f**2*ui**2))/((2*pi*C*f*er-(2*pi*L*f*ur)/(4*pi**2*L**2*f**2*ur**2+4*pi**2*L**2*f**2*ui**2))**2+((2*pi*L*f*ui)/(4*pi**2*L**2*f**2*ur**2+4*pi**2*L**2*f**2*ui**2)+2*pi*C*f*ei)**2))

	def Zr(self,f):#Cálculo de la parte real de la impedancia a la frecuencia f
		r=self.R

		l=self.L
		ur=self.uR
		ui=self.uI

		c=self.C
		er=self.eR
		ei=self.eI

		return(self.ZEQr(f,l,c,r,ur,ui,er,ei))

	def Zx(self,f):#Cálculo de la parte imaginaria de la impedancia a la fre cuencia f
		r=self.R

		l=self.L
		ur=self.uR
		ui=self.uI

		c=self.C
		er=self.eR
		ei=self.eI

		return(self.ZEQx(f,l,c,r,ur,ui,er,ei))

	def Z(self):#Cálculo de la parte real e imaginaria de la impedancia
		if isinstance(self.F, list):#Si la respuesta se busca para un arrray de frecuencias
			self.zR=[]
			self.zX=[]
			for i in range(0,len(self.F)):
				self.zR.append(self.Zr(self.F[i]))
				self.zX.append(self.Zx(self.F[i]))

		else:
			self.zR=self.Zr(self.F)
			self.zX=self.Zx(self.F)

	def Nr(self):
		return math.cos(0.5*(math.atan2(self.uI,self.uR)+math.atan2(self.eI,self.eR)))*(math.sqrt(self.eR**2+self.eI**2)*math.sqrt(self.uR**2+self.uI**2))**0.5
	
	def Ni(self):
		return math.sin(0.5*(math.atan2(self.uI,self.uR)+math.atan2(self.eI,self.eR)))*(math.sqrt(self.eR**2+self.eI**2)*math.sqrt(self.uR**2+self.uI**2))**0.5

	def N(self):
		self.nR=self.Nr()
		self.nI=self.Ni()


class OPTBP:#Optimizador de bobina plana real a modelo
	Z0r=50#Parte real de la impedancia característica
	Z0i=0#Parte imaginaria de la impedancia cvaracterística

	mL=1e-9#Multiplicador de la aiutoinductancia "nH"
	mC=1e-12#Multiplicador capacitancia "pF"
	#mL=1e-7#Multiplicador de la aiutoinductancia "nH"
	#mC=1e-10#Multiplicador capacitancia "pF"

	def __init__(self, rbp, mbp, er0=1, ur0=1):
		self.RBP=rbp#Real planar inductor objects
		self.MBP=mbp#Model of planar inductor

		self.GUSeR=er0#Base effective permittivity gess
		self.GUSuR=ur0#Base effective permeability gess

		self.ERR=0#Current optimization error

		self.IVAR=[]#Model initial variables
		self.VARLM=[]#Variable limits
		self.OMVAR=[]#optimiced model variables

		self.FI=0#Initial frequency for error calculation
		self.FF=0#Final frequency for error calculation
		self.nFI=0#n array value of initial frequency for error calculation
		self.nFF=0#n array value of final frequency for error calculation

		#Optimizator variable initialization
		self.VARIN()
		#Optimizator variable initialization

	def ERRF(self):#Error function
		self.ERR=0
		for i in range(0,len(self.RBP)):#Iterator for all opt cases

			#self.MBP[i].F=self.RBP[i].F
			#self.MBP[i].Z()

			for ii in range(self.nFI,self.nFF):#Iterator for all frequency values at this opt case
				self.MBP[i].F=self.RBP[i].F[ii]
				self.MBP[i].Z()

				self.ERR=self.ERR+(self.RBP[i].zR[ii]-self.MBP[i].zR)**2
				self.ERR=self.ERR+(self.RBP[i].zX[ii]-self.MBP[i].zX)**2

	def VARIN(self):#Models variable initiator

		#Clean variable arrays
		self.IVAR=[]
		self.VARLM=[]

		#Calculate optimization limits
		self.RBP[0].CSRF()

		self.FI=self.RBP[0].SRF[0]*0.1
		self.FF=self.RBP[0].SRF[0]*1.2
		self.nFI=MMRF.FND_V_L(self.RBP[0].F,self.FI)[0]
		self.nFF=MMRF.FND_V_L(self.RBP[0].F,self.FF)[0]

		#Calculate an initializate autoinductance
		self.RBP[0].LFCL()

		self.IVAR.append(self.RBP[0].LFL/(self.mL*self.GUSuR))
		self.VARLM.append((self.IVAR[0]*0.5,self.IVAR[0]*1.25))

		#Calculate an initializate capacitance
		self.RBP[0].LFCC()
		self.IVAR.append(self.RBP[0].LFC/(self.mC*self.GUSeR))
		self.VARLM.append((self.IVAR[1]*0.1,self.IVAR[1]*10))

		#Calculate and initiallizate resistance
		self.RBP[0].LFCR()
		self.IVAR.append(self.RBP[0].LFR)
		self.VARLM.append((0,self.RBP[0].LFR*1e2))

		#Initialization of permeability and permittivity
		for i in range(0,len(self.RBP)):
			if(i==0):#First data set "BLANK"
				self.IVAR.append(1*self.GUSuR)#Ur
				self.VARLM.append((0.999,1.001))

				self.IVAR.append(1e-4)#Ui
				self.VARLM.append((1e-100,1))

				self.IVAR.append(1*self.GUSeR)#Er
				self.VARLM.append((0.999,2*self.GUSeR))

				self.IVAR.append(1e-3)#Ei
				self.VARLM.append((1e-100,1))
			else:
				self.IVAR.append(1*self.GUSuR)#Ur
				self.VARLM.append((0.999,1.5*self.GUSuR))

				self.IVAR.append(1e-4)#Ui
				self.VARLM.append((1e-100,1))

				self.IVAR.append(1*self.GUSeR)#Er
				self.VARLM.append((0.999,2*self.GUSeR))

				self.IVAR.append(1e-3)#Ei
				self.VARLM.append((1e-100,1))



	def VARCOM(self,mvar):#Vaiable copositor. puts var array on models
		for i in range(0,len(self.MBP)):
			self.MBP[i].L=mvar[0]*self.mL
			self.MBP[i].C=mvar[1]*self.mC
			self.MBP[i].R=mvar[2]

			self.MBP[i].uR=mvar[3+i*4]
			self.MBP[i].uI=mvar[4+i*4]
			self.MBP[i].eR=mvar[5+i*4]
			self.MBP[i].eI=mvar[6+i*4]

			self.MBP[i].N()#Calculation of refraction index

			#print(self.RBP[i])
			#print(self.MBP[i])



	def OPTFUNCT(self,VARIB):#Function to pass to the optimicer
		self.VARCOM(VARIB)
		self.ERRF()

		return self.ERR

	def callbackOPT(self, xk):
		print(str(self.OPTFUNCT(xk))+"\n")
		print(str(xk)+"\n")


	def OPT(self):#Optimization routine
		R_OPT = minimize(self.OPTFUNCT,x0=self.IVAR, bounds=self.VARLM, method='L-BFGS-B', callback=self.callbackOPT, options={'disp': True, 'maxcor': 100, 'ftol': 1e-100, 'gtol': 1e-100, 'maxls': 200, })#////////////////////////////////////////// , callback=callbackOPT
		print("R_OPT")
		print(R_OPT)
		print("\n")
		self.OMVAR=R_OPT.x



class SRFM:#Solver for mass calculation on SRF of the planar inductor method
	#
	def __init__(self, srf, esrf, mass): #

		self.MS=list(mass)
		self.SRF=list(srf)
		self.eSRF=list(esrf)
		#self.SRF=LNAL.SxV(1e-9,srf)#Frecuencia de resonancia::Una SRF por masa
		#self.eSRF=LNAL.SxV(1e-9,esrf)#Error of SRF

		#Condition of limit when ms->inf then SRF->0
		#self.SRF.append(1e-99)
		#self.MS.append(1e99)

		##OPT SOLVER VARIABLES
		self.NMODFUNC=0
		self.An=[0,0,0]#Constants for model calculation
		self.An0=[1, 1, 1, 1, 1]#Real parameters
		self.LCN0=[(1e-50,1e50),(1e-50,1e50),(1e-50,1e50),(1e-50,1e50),(1e-50,1e50)]
		##OPT SOLVER VARIABLES

		self.mSRF=[]#Modeled Frequency
		self.mMS=[]#Modeled mass

		#print(self.ms)
		#print(max(self.ms))

		#self.OPTSOLV()

		#self.FITTSOLV()

		#self.INTERPSOLV()
		#self.NMODFUNC=self.NMOD_P

		self.INTERPLICH()
		self.NMODFUNC=self.NMOD_Hs_PL

		#self.INTERP0SOLV()

		print("An parameters:")
		print (self.An)

		self.rGFIT(self.MS)

		RS=STAT.RSQU(self.SRF,self.mSRF)
		print("R²:"+str(RS))

		self.GFIT(0,max(self.MS),4000)

			#__debug__
	def NMOD_P(self,mass,poll):#Refraction index MODEL POLINOMIC
		N=0

		for i in range(0,len(poll)):
			N=N+(poll[i]*(mass**(len(poll)-i-1)))

		return N

	def NMOD_L(self,mass,params):#Refraction index MODEL LOGARITHMIC
		N=(params[0]*math.log((mass+params[1]),math.e))+(mass**2)*params[2]+mass*params[3]+params[4]

		return N

	def NMOD_F(self,mass,params):#Refraction index MODEL POW SERIES
		msr=params[0]+mass
		N=(params[1]/msr)+params[2]+(params[3]*msr)+(params[4]*(msr**2))

		return N

	def NMOD_H(self,mass,params):#Refraction index Lichtenecker’s mixture formulae
		#vars 	uENV, eENV, uMNP, eMNP
		uENV=1#environment magnetic permeability
		eENV=3#environment electric permittivity
		uMNP=10#MNP magnetic permeability
		eMNP=10#MNP electric permittivity
		km=1#magnetic general k value
		ke=-1#electric general k value
		fm=0.06939204569815902#magnetic sample volume fraction of 95% field
		fe=0.032641222246117645#electric sample volume fraction of 95% field
		MXmass=1000#Maximum possible mass
		R=mass/MXmass
		R=R*params[0]#Volume modifier for simplification

		uS=math.e**((R*math.log(uMNP,math.e))+((1-R)*math.log(uENV,math.e)));
		eS=math.e**((R*math.log(eMNP,math.e))+((1-R)*math.log(eENV,math.e)));

		uEFF=uS
		eEFF=eS

		#uEFF=((fm*(uS**km))+((1-fm)*(uENV**km)))**(1/km)
		#eEFF=((fe*(eS**ke))+((1-fe)*(eENV**ke)))**(1/ke)

		LC=params[1]

		N=(uEFF*eEFF)*LC

		return N

	def NMOD_Hs(self,mass,params):#Refraction index Lichtenecker’s mixture formulae SIMPLIFIED
		R=(mass/1000)*params[0]#Volume modifier for simplification
		nMNP=params[1]#MNP refraction index
		nENV=params[2]#environment refraction index
		
		nEFF=nENV*(nMNP/nENV)**R#product of permeability and permittivity

		
		#nEFF=math.e**(params[0]*mass*params[1]+params[2])
		
		LC=params[3]
		N=nEFF*LC

		return N

	def NMOD_Hs_PL(self,mass,params):#Refraction index Lichtenecker’s mixture formulae SIMPLIFIED polinomic
		logN=mass*params[0]+params[1]
		N=math.e**(logN)
		return N

	def SRFMOD(self,mass,nmod,params):#Self resonan frequency MODEL
		return 1/(((nmod(mass,params))**0.5)*2*pi)

	def GFIT(self, m0, mf, nsteps):#Generate data from model
		self.mMS=[]
		self.mSRF=[]

		for i in frange(m0,mf,(mf-m0)/nsteps):#x value, mass
			self.mMS.append(i)#Mass
			self.mSRF.append(self.SRFMOD(i,self.NMODFUNC,self.An))

	def rGFIT(self, xl):#Generate data from model
		self.mMS=list(xl)
		self.mSRF=[]

		for i in range(len(self.mMS)):#x value, mass
			self.mSRF.append(self.SRFMOD(self.mMS[i],self.NMODFUNC,self.An))


	#########################################################################################
	def SRFERR(self,params):
		ERR=0
		for i in range(len(self.MS)):
			ERR=ERR+(self.SRF[i]-self.SRFMOD(self.MS[i],self.NMODFUNC,params))**2

		return ERR

	def callbackOPT(self, xk):
		print(str(self.CALIBFUNC(xk))+"\n")
		print(str(xk)+"\n")

	def OPTSOLV(self):#From ms and SRFm calculates parameters of general expression
		assert len(self.SRF)>=3 ,"At least 3 masurements are necesary for parameter calculation"

		##Defining N MOD
		self.NMODFUNC=self.NMOD_L
		self.An0=[1, 1, 1, 1, 1]#Real parameters
		self.LCN0=[(1e-4,1e4),(1e-4,1e2),(1e-4,1e4),(1e-4,1e4),(1e-4,1e4)]
		##Defining N MOD

		##Defining N MOD
		self.NMODFUNC=self.NMOD_Hs
		self.An0=[1e-5,1,1,1]#Real parameters
		self.LCN0=[(1e-10,1),(0.99,100),(0.99,100),(1,1e7)]
		#self.An0=[0.1,0.1, 1]#Real parameters
		#self.LCN0=[(0,1),(0,1),(0.00001,10000000)]
		#self.An0=[1,3,10,10, 1]#Real parameters
		#self.LCN0=[(0.99,10),(0.99,10),(0.99,100),(0.99,100),(0.00001,10000000)]
		##Defining N MOD


		#CN0=[1.56770278e-27, 5.43372642e-23, 1.69040461e-20]#Real parameters
		
		#Global

		nSTPS=5
		CASES=[]
		for i in range(len(self.LCN0)):#Iterrator for all variables
			STEPS=(self.LCN0[i][1]-self.LCN0[i][0])/nSTPS
			CASES.append(frange(self.LCN0[i][0],self.LCN0[i][1],STEPS))
		print("CASES")
		print(CASES)
		#Optimization initial condition cases defined

		#Cases iterator
		COMBS=COMB.COMBL(CASES)
		print("COMBS")
		print(COMBS)

		ERRMIN=self.SRFERR(self.An0)
		print("INIT ERR: "+str(ERRMIN))
		for i in range(len(COMBS)):
			self.An0=list(COMBS[i])
			print("COMB:")
			print(self.An0)
			R_CALIB = minimize(self.SRFERR,x0=self.An0, bounds=self.LCN0, method='L-BFGS-B', options={'maxcor': 10000, 'ftol': 1e-10000, 'gtol': 1e-6000, 'maxls': 30000, 'disp': False } )

			tAn=list(R_CALIB.x)
			print("TEMPSOL")
			print(tAn)
			ERR=self.SRFERR(tAn)
			print("ERROR::"+str(ERR))
			if(ERR<ERRMIN):
				ERRMIN=ERR
				self.An=list(tAn)
				print("SOLUTION FOUND")
				print(self.An)


		print("An")
		print(self.An)
		print("Error")
		print(ERRMIN)
		#self.An=list(R_CALIB.x)

		"""
		rranges = (slice(1e-50, 1, 0.25), slice(0, 100, 10), slice(0, 100, 10), slice(1e-4,1e7, 1000))
		resbrute = optimize.brute(self.SRFERR, rranges, full_output=True, finish=optimize.fmin)
		print (resbrute)
		"""

		"""
		R_CALIB = minimize(self.SRFERR,x0=self.An0, bounds=self.LCN0, method='L-BFGS-B', options={'maxcor': 10000, 'ftol': 1e-10000, 'gtol': 1e-6000, 'maxls': 20000, 'disp': False } )

		self.An=list(R_CALIB.x)
		"""
		

	#############################################################################################
	def INTERPSOLV(self):#Interpolation model solver
			def yDINT(f):
				return (1/(2*pi*f)**2)

			ydint=[]

			for i in range(0,len(self.SRF)):
				ydint.append(yDINT(self.SRF[i]))

			#print("INTERP_SRF")
			#print(self.SRF)

			#print("INTERP_MS")
			#print(self.MS)

			self.An=np.polyfit(self.MS,ydint,2)

			"""#__debug__
			popt, pcov = curve_fit(self.SRFMOD, self.ms, self.SRF, bounds=([0,0,0], [1,1,1]),p0=list(self.LCCN))
			print("popt")
			print(popt)
			#"""
			#print("POLIFIT")
			#print(self.LCCN)
	def INTERP0SOLV(self):#interpolation with 0 mass data
		def yDINT(srf,m):
			return (1/(((2*pi*srf)**2)*m))

		ydint=[]
		MS=[]
		A0=0

		for i in range(0,len(self.SRF)):
			if(self.MS[i]!=0):
				ydint.append(yDINT(self.SRF[i],self.MS[i]))
				MS.append(self.MS[i])

		FOUND=[]
		for i in range (len(self.MS)):#Check for 0 mass
			if (self.MS[i]==0):#If exist mass 0
				A0=(1/(2*pi*self.SRF[i])**2)
				break




		#print("INTERP_SRF")
		#print(self.SRF)

		#print("INTERP_MS")
		#print(self.MS)

		tAn=np.polyfit(MS,ydint,1)

		"""#__debug__
		popt, pcov = curve_fit(self.SRFMOD, self.ms, self.SRF, bounds=([0,0,0], [1,1,1]),p0=list(self.LCCN))
		print("popt")
		print(popt)
		#"""
		#print("POLIFIT")
		#print(self.LCCN)
		self.An=[]
		for i in range(len(tAn)):
			self.An.append(tAn[i])
		self.An.append(A0)

	def INTERPLICH(self):#Interpolation of lichteneker
		def yDINT(f):
			return math.log((1/(2*pi*f)**2),math.e)

		ydint=[]

		for i in range(0,len(self.SRF)):
			ydint.append(yDINT(self.SRF[i]))

		#print("INTERP_SRF")
		#print(self.SRF)

		#print("INTERP_MS")
		#print(self.MS)

		self.An=np.polyfit(self.MS,ydint,1)

	##############################################################################################
	def FITFUNCT(self, x, a2, a1, a0):
		return self.SRFMOD(x,[a2,a1,a0])

	def FITTSOLV(self):
		An0=[1, 1, 1]#Real parameters
		self.An, pcov = curve_fit(self.FITFUNCT, self.MS, self.SRF, bounds=([0,0,0], [1e100,1e100,1e100]),p0=list(An0))
			#print("popt")
			#print(popt)
			#"""
			#print("POLIFIT")
			#print(self.LCCN)

	##############################################################################################


"""
	def xDINT(self, m, lccn):
		return ((lccn[0]+lccn[1]*m+lccn*m**2))

	def n(self,m,cn):#Relative refraction index calculation
		return (cn[0]+cn[1]*m+cn[2]*m**2)**0.5

	def srf(self,m,cn,lc):#Calculation of SRF from values
		return(1/(2*pi*(lc+self.n(m,cn))**0.5))

	def RELATm(m0,m1):
		return(self.n(m0)/self.n(m1))#=SRF0/SRF1

	def CALIBFUNC(self,X):#Function for solver
		RESS=0
		cn=MMRF.SdLIST(X,1e-20)

		for i in range(0,len(self.SRF)):
			for ii in range(i+1,len(self.SRF)):
				tRESSa=self.n(self.ms[i],cn)/self.n(self.ms[ii],cn)
				#print("REL N:"+str(i)+"::"+str(ii)+":"+str(tRESSa)+"\n")
				tRESSb=self.SRF[ii]/self.SRF[i]
				#print("REL SRF:"+str(i)+"::"+str(ii)+":"+str(tRESSb)+"\n")
				RESS=RESS+((tRESSa-tRESSb)**2)*1e10


		return RESS

	def CALIBFUNC_2(self,X):
		RESS=0
		cn=X[0:2]
		lc=X[3]

		for i in range(0,len(self.SRF)):
			RESS=RESS+(self.SRF[i]-self.srf(self.ms[i],cn, lc))**2

		return RESS

#"""



