# -*- coding: utf-8 -*-
#!/usr/bin/env python

#SRFI.py -> Self resonant frequency identifier

#SRF Methods
import sys
sys.path.append('..')

import math
import cmath

from SUBM.MATH.PY import INTRP
from SUBM.MATH.PY import LNAL


def IDSRF(frq,z):#Returns the array of ids of the 2 values surrounding the SRF points
	srfid=[]
	for i in range (1,len(frq)):
		if((z[i-1].imag>=0 and z[i].imag<=0)):# Resonant point found
			srfid.append([i-1,i])
	return srfid

def SRCHSRF(frq,z):
	srf=[]
	srfid=IDSRF(frq,z)
	for i in range (len(srfid)):
			srf.append((INTRP.LIN_INTERP_Fx(frq[srfid[i][0]],frq[srfid[i][1]],z[srfid[i][0]].imag,z[srfid[i][1]].imag,0)))

	return srf

def RTRZSRF(frq,z):
	z=[]
	srfid=IDSRF(frq,z)
	for i in range (len(srfid)):
			srf.append((INTRP.LIN_INTERP_Fx(z[srfid[i][0]],z[srfid[i][1]],z[srfid[i][0]].imag,z[srfid[i][1]].imag,0)))

	return z

def SRCH_SRF(Frq,Zi):
	srf=[]
	for i in range (1,len(Frq)):
		if((Zi[i-1]>=0 and Zi[i]<=0)):# Resonant point found
			#print("i-1="+str(i-1)+" i="+str(i))
			srf.append((INTRP.LIN_INTERP_Fx(Frq[i-1],Frq[i],Zi[i-1],Zi[i],0)))

	return srf

def Zr_SRF(srf,Zr,Frq):
	Zr_srf=[]
	for i in range(0,len(srf)):
		Zr_srf.append(Zr[LNAL.FNDEL(Frq,srf[i])[0]])
	return Zr_srf

def Qf_SRF(srf,Zr,Frq):
	Qf_srf=[]
	for i in range(0,len(srf)):
		Zr_srf=Zr_SRF(srf,Zr,Frq)[0]
		n_Zr_srf_3dB=LNAL.FNDEL(Zr,Zr_srf/(2**0.5))
		srf_3db=Frq[n_Zr_srf_3dB[0]]
		Qf_srf.append(srf[i]/abs(2*(srf[i]-srf_3db)))
	return Qf_srf

#SRF Methods
