# -*- coding: utf-8 -*-
#!/usr/bin/env python
#!/usr/bin/python -u

#SIM_IMP_DTPR -> Simulations impedance data process.


import sys
sys.path.append('..')
from PY import TSFI
from PY import SRFI

def TEST():
	print("RFU_TEST")


	print("TSFI_TST")

	VNAD=[TSFI.TOUCHST("TOUCHS_0.s2p"),TSFI.TOUCHST("TOUCHS_1.s2p")]#VNA data.
	VNAD[0].HEDPR()
	VNAD[1].HEDPR()

	VNAD[0].MTXC()
	VNAD[1].MTXC()

	print("VNAD.IMP")
	print(VNAD[0].IMP)
	print(VNAD[1].IMP)

	print("SRFI_TST")
	print(SRFI.SRCHSRF(VNAD[0].FRQ,VNAD[0].IMP))
	print(SRFI.SRCHSRF(VNAD[1].FRQ,VNAD[1].IMP))


if __name__ == '__main__':
	TEST()
